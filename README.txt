IP Block
========
  Detect IP (v4) addresses used by bots, and block these from posting or logging in
  Can dramatically reduce comment spam
  To get the best out of this you need to manually modify .htaccess and robots.txt - see below

Author & Acknowledgements
===========================
	devel.tdk@squte.com
	This module uses some code from the IP_login module

Features
========
  * When enabled automatically stops logins, and posting of comments by detected bots. 
  * In case of false positives, users are informed when they are blocked and can automatically unblock themselves by completing a simple captcha
  * Provides a block that can be used to show the users 'blocked' status.


How it works
============
  The module sets up a 'bot trap' aka honeypot to trap bots, and records their IP addresses
  
  1. It inserts a link to /ip_block/block_me in each node. This is hidden from most users but visible to bots.  
  2. Any visit to 'block_me' results in the IP address being added to a table. If there are any anonymous comments that have been made from that IP, they are deleted.  
  3. This table is checked before allowing comments or logins.
  For more details please see this post: squte.com/bot_trap

Installation
============

  Minimal
  ------------
  1. Enable the module from Drupal admin -> modules
  2. Configure the navigation menu to remove the 'Block me' item.
  
  Extra
  ------------
  3. Change the 'captcha' to a custom one.
  4. update robots.txt
		By default legitimate bots like googlebot may be blocked. ATM this is not a problem because these shouldn't be posting anyway. But if in future content is blocked then it makes sense to only detect 'bad' bots. This is done by adding this to robots.txt:
		# set up bot trap  
		User-agent: *
		Disallow: /ip_block/
		
  5. update .htaccess
  
		You can also add the following to your .htaccess file - this will block bots scanning for and attempting various attacks. Put these in the <IfModule mod_rewrite.c> section:
			# one forum spammer script has bug in it that puts lots of '+'s in url - make sure are blocked
			RewriteRule ^(.*)\+\+\+[[Rr]]esult(.*)$ /ip_block/block_me [[L]]
			RewriteRule ^(.*)\+script\+security(.*)$ /ip_block/block_me [[L]]
			# catch any attempts to hack or probe the site by redirecting to bot blocker
			# these should also be blocked in robots.txt, just to ensure we dont get a genuine bot 
			RewriteRule ^/wp-login.php(.*)$ /ip_block/block_me [[L]]
			RewriteRule ^/signup.php(.*)$ /ip_block/block_me [[L]]
			RewriteRule ^/tiki-register.php(.*)$ /ip_block/block_me [[L]]
			RewriteRule ^/CHANGELOG.txt$ /ip_block/block_me [[L]]
			RewriteRule ^/YaBB.cgi(.*)$ /ip_block/block_me [[L]]
			RewriteRule ^/YaBB.pl(.*)$ /ip_block/block_me [[L]]
			RewriteRule ^/account/register.php(.*)$ /ip_block/block_me [[L]]  
			RewriteRule ^/join.php(.*)$ /ip_block/block_me [[L]]
			RewriteRule ^/member/register(.*)$ /ip_block/block_me [[L]]
			RewriteRule ^/node/community/register.html(.*)$ /ip_block/block_me [[L]]
			RewriteRule ^/node/pg/register/(.*)$ /ip_block/block_me [[L]]
			RewriteRule ^/pg/register/(.*)$ /ip_block/block_me [[L]]
			RewriteRule ^/register.php(.*)$ /ip_block/block_me [[L]]
			RewriteRule ^/registration_rules.asp(.*)$ /ip_block/block_me [[L]]
			RewriteRule ^/site/signup.php(.*)$ /ip_block/block_me [[L]]
			
		If you make the above change then update your robots.txt again, add this:
		
			# block any that are redirected in .htaccess to bot trap just to ensure genuine bots don't visit them 
			User-agent: *
			Disallow: /wp-login.php
			Disallow: /signup.php
			Disallow: /tiki-register.php
			Disallow: /CHANGELOG.txt
			Disallow: /YaBB.cgi
			Disallow: /YaBB.pl
			Disallow: /account/register.php  
			Disallow: /join.php
			Disallow: /member/register
			Disallow: /node/community/register.html
			Disallow: /node/pg/register/
			Disallow: /pg/register/
			Disallow: /register.php
			Disallow: /registration_rules.asp
			Disallow: /site/signup.php

How to test
===========
  1. Visit the 'Unblock me' menu, you should see a message your ip is not blocked
  2. View the html source for a node, you should see a link to /ip_block/block_me.
  3. Visit /ip_block/block_me. You will see a blank page
  4. Go to any other page, you should see a message that your ip is now blocked
  5. Try to login or post a comment - this should fail.
  6. Visit the 'Unblock me' menu, complete the captcha. You should now be able to login and comment

Enhancements
==================================
	There are patches available for the spambot, Botcha, and the vote up down modules.
	The first two block the IP address on a failed comment attempt. 
	Typically when a spambot fails to post, it tries again testing for various defences eg trying wihout links, without spam keywords etc.
	It will typically try about 10 times. Examining the logs shows most comment spam is preceded by failed attempts. Blocking the ip on the first attempt doesn't give the spammers any 2nd chances.
	SpamBot patch
		In the file spambot.module in the function spambot_comment_validate, immediately after 'form_set_error('comment', $message);', add the following code: 
			// block ip address if spammer 
			if (module_exists('ip_block')) {
				ip_block_add(ip_address());
			}      
		Do the same in the function spambot_user_register_validate, immediately after the line "watchdog('spambot', ..."
	Botcha patch
		In the file botcha.recipebook.controller.inc, in the function "public function handle", immediately after the line "$rules_event_name = 'botcha_form_rejected';", add the same code.
	Vote up down patch
		In vud.module, in the function vud_access_callback, immediately before the line "return TRUE;" add this:
		  // dont allow blocked IPs to vote
			if (module_exists('ip_block')) {
				if (ip_block_blocked()) {
					return FALSE;
				}
			}
		this will stop bots from voting.


