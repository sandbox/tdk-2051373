<?php
/**
 * @file
 * Sets up the IP Block admin page settings.
 */

/**
 * Returns the settings form
 */
function ip_block_admin_settings() {
  $form['ip_address'] = array(
    '#prefix' => '<div class="messages status">',
    '#description' => t('IP addresses blocked are listed below.'),
    '#value' => t('Your IP address is <strong><code>@ip_address</code></strong>.</div>', array('@ip_address' => ip_address())),
    '#weight' => '0'
  );

  $form['ip_block_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('IP Block enabled'),
    '#description' => t('Check to enable IP Block, uncheck to disable. If unchecked no IP addresses will be blocked.'),
    '#default_value' => variable_get('ip_block_enabled', 1)
  );

  //TODO: not sure what this is 
  $use_token = module_exists('token');
  if ($use_token) {
  	$form['other_messages'] = array(
			'#type' => 'fieldset',
			'#title' => t('Messages'),
			'#description' => t('You can alter other messages here.'),
		);
  
    $form['other_messages']['tokens'] = array(
      '#value' => theme('token_tree', array('user'), TRUE, TRUE),
    );
  }  
  
	$form['captcha_question'] = array(
		'#type' => 'textarea',
		'#default_value' => variable_get('captcha_question', 'Type \'real person\' here'),   
		'#title' => t('captcha_question') ,
		'#description' => t('captcha_question'),
		'#maxlength' => 255
	);
	
	$form['captcha_answer'] = array(
		'#type' => 'textarea',
		'#default_value' => variable_get('captcha_answer', 'real person'),   
		'#title' => t('captcha_answer') ,
		'#description' => t('captcha_answer'),
		'#maxlength' => 255
	); 
	
	// wrap in a fieldset
	$form['ip_block_matches'] = array(
		'#type' => 'fieldset',
		'#title' => t('IP Block'),
		'#description' => t('Blocked by IP address.'),
		'#weight' => '0'
	);
	
	$form['ip_block_matches']['ip_block_add'] = array(
		'#type' => 'textarea',
		'#default_value' => FALSE,   
		'#return_value' => TRUE,
		'#rows' => 1,
		'#title' => t('Add block for IP address') ,
		'#description' => t('IPv4 address'),
		'#maxlength' => 16
	);
 
	$form['ip_block_matches']['ip_block_remove'] = array(
		'#type' => 'textarea',
		'#default_value' => FALSE,   
		'#return_value' => TRUE,
		'#rows' => 1,
		'#title' => t('Remove block for IP address') ,
		'#description' => t('IPv4 address'),
		'#maxlength' => 16
	);
	      	 
       /*
      $sql =  "SELECT uid, ip_match
       FROM {ip_block}
       ORDER by ip_match ASC";
        $matches = pager_query($sql, 20, NULL);
        
     
				
			while ($row = db_fetch_object($matches)) {
				$form['ip_block_matches']['ip_block_del' . $row->uid] = array(
					'#type' => 'checkbox',
					'#default_value' => FALSE, 
					'#return_value' => TRUE,
					'#title' => t('Delete block for IP address: ') . $row->ip_match,
					'#description' => t('IP matches based in format listed above.')
					//'#maxlength' => 255
				);
			}
  */
  $form['#submit'][] = 'ip_block_admin_settings_submit';
  return system_settings_form($form);
}

/*
 * Themes the header of the table
 *
 * @return
 *   An array suitable for use with tablesort_sql
 */
function ipblock_list_header() {
  return array(
  	array('data' => t('UID'), 'field' => 'ip_block.uid'),
    array('data' => t('IP match'), 'field' => 'ip_block.ip_match'),
  );
}

/*
 * Themes the output of a single row
 * @return
 *   Array for a single row, suitable for inclusion with theme_table
 */
function ipblock_list_row($row) {
  return array(
  	array('data' => $row->uid, 'align' => 'left'),
    array('data' => $row->ip_match, 'align' => 'left'),
  );
}

function do_ip_block_list($header, $rows, $pager_limit) {
    $output = theme('table', $header, $rows);
    $output .= theme('pager', NULL, $pager_limit, 0);  
  return $output;
}

/*
 * Purpose: todo
 * Returns: HTML from the theme function
 */
function ip_block_list() {

  $sql = "SELECT uid, ip_match FROM {ip_block}";
  $sql_cnt = "SELECT COUNT(DISTINCT(uid))  FROM {ip_block}";

  $header = ipblock_list_header();//todo:theme
  $sql .= tablesort_sql($header);
  $pager_limit = 30;
  $result = pager_query($sql, $pager_limit, 0, $sql_cnt);

  $rows = array();

  while ($data = db_fetch_object($result)) {
    $rows[] = ipblock_list_row($data);//todo:theme
  }

  drupal_set_title("IP Block List");
  return do_ip_block_list( $header, $rows, $pager_limit);//todo:theme
}

/**
 * Submit handler for admin settings form
 */
function ip_block_admin_settings_submit($form, &$form_state) {
  /*
  $matches = db_query(
		"SELECT  ip_match, uid
		 FROM {ip_block}"     
		);
			
	while ($row = db_fetch_object($matches)) {
			//watchdog('debug', 'ip_block set %uid ', array('%uid' =>  $row->uid));
			//watchdog('debug', 'ip_block edit "%ed" ', array('%ed' =>  $form_state['values']['ip_block_del' . $row->uid]));
			
			if ( $form_state['values']['ip_block_del' . $row->uid]  == TRUE) {
				//watchdog('debug', 'ip_block del "%uid" ', array('%uid' =>  $row->uid));
				ip_block_delete($row->ip_match);
			}
	}
	*/
	//watchdog('debug', 'ip_block_match "%match" ', array('%match' =>  $form_state['values']['ip_block_match']));
	if ( !empty($form_state['values']['ip_block_add']) ) {			
      ip_block_add($form_state['values']['ip_block_add'], FALSE );
  }
  if ( !empty($form_state['values']['ip_block_remove']) ) {		
      ip_block_delete($form_state['values']['ip_block_match'] );
  }  
}

